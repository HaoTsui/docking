﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Docking_2D_scenario_1 
 * Rotation is only around y-axis 
 * Motion is fixed in xz plane (fixed y coordinate)
 * Discrete
 * 
 * TODO: 
 * Docking_2D_scenario_2
 * Motion is fixed in xy plane (fixed z coordinate)
 * Punishment: 1. constant y positive/negative rotation 2. constant x^2 + y^2 increase (away from the target)
 */
public class Docking2DAgent : Agent
{
    // Spaceship states
    private float spaceshipPosX;
    const float spaceshipPosY = 0;  // fix to xz plane
    private float spaceshipPosZ;
    private float spaceshipRotY;    // Euler angle (Degree)

    // Space station states
    private float spaceStationPosX;
    const float spaceStationPosY = 0; // fix to xz plane
    private float spaceStationPosZ;

    // Hyper parameter
    const int movementScale = 1;
    const float rotationDegree = 10f; // degree!
    static private float initPosRange = 10;
    static private float rLimit = Mathf.Sqrt(Mathf.Pow(3 * initPosRange, 2)); // (2*initPosRange* root(2))^2 -> the possible largest initialization distance
    const float degree2Rad = 0.0174533f; // pi/180

    // Historical 
    private float previousR= rLimit;
    private float previousAngleDiff = 359 * degree2Rad;
    private int stepsCount = 0;
    bool isMove = false;
    bool isRotate = false;

    // Visibility
    [SerializeField]
    private Transform spaceStation;
    [SerializeField]
    private Transform spaceship;

    // Tracing
    [SerializeField]
    private UnityEngine.UI.Text text;
    private float successCount = 0;
    private float failureCount = 0;

    public override void CollectObservations()
    {
        AddVectorObs(spaceshipPosX);
        AddVectorObs(spaceshipPosY);
        AddVectorObs(spaceshipPosZ);
        AddVectorObs(spaceshipRotY);
        AddVectorObs(spaceStationPosX);
        AddVectorObs(spaceStationPosY);
        AddVectorObs(spaceStationPosZ);
    }

    // Helper functions
    private float calcStationAngle(float x_diff, float z_diff, float distance)
    {
        //TODO: Here is nasty hack code; however, the trained performance is good 
        Vector2 vecShip2Station = new Vector2(x_diff, z_diff);
        Vector2 unitVecShip2Station = vecShip2Station / distance; // Normalize
        Vector2 z_axis = new Vector2(0, 1); // Unit vector 
        float vectorDot = Vector2.Dot(z_axis, unitVecShip2Station);
        float stationToZaxisAngleRadian = Mathf.Acos(vectorDot); // In radian
        float stationToZaxisAngleDegree;
        if (x_diff > 0) // 0 ~ 180 degree
        {
            stationToZaxisAngleDegree = stationToZaxisAngleRadian / degree2Rad; // In degree
        }
        else // 180 ~ 360 degree
        {
            stationToZaxisAngleDegree = 360 - (stationToZaxisAngleRadian / degree2Rad);
        }

        return stationToZaxisAngleDegree;
    }

    /*
     * 0: Up    Arrow: positive z 
     * 1: Down  Arrow: negative z 
     * 2: Right Arror: positive x
     * 3: Left  Arror: negative x
     * 4: Q     Key:   positive rotation y
     * 5: W     Key:   negative rotation y
     */

    // Discrete
    public override void AgentAction(float[] act, string textAction)
    {
        int action = (int)act[0];
        float radian = spaceshipRotY * degree2Rad;

        // Z position
        if (action == 0 || action == 1)
        {
            stepsCount++; //increament the steps for any motion
            isMove = true;
            if (action == 0)
            {
                spaceshipPosX += movementScale * Mathf.Sin(radian);
                spaceshipPosZ += movementScale * Mathf.Cos(radian);
            }
            else
            {
                spaceshipPosX -= movementScale * Mathf.Sin(radian);
                spaceshipPosZ -= movementScale * Mathf.Cos(radian);
            }
        }

        // X position
        if (action == 2 || action == 3)
        {
            stepsCount++; //increament the steps for any motion
            isMove = true;
            if (action == 2)
            {
                spaceshipPosX += movementScale * Mathf.Cos(radian);
                spaceshipPosZ -= movementScale * Mathf.Sin(radian);
            }                
            else
            {
                spaceshipPosX -= movementScale * Mathf.Cos(radian);
                spaceshipPosZ += movementScale * Mathf.Sin(radian);
            }        
        }

        // Y rotation
        if (action == 4 || action == 5)
        {
            stepsCount++; //increament the steps for any motion
            isRotate = true;
            if (action == 4)
            { 
                spaceship.transform.Rotate(Vector3.up, rotationDegree); // Rotate rotationDegree degree
                spaceshipRotY = spaceship.transform.eulerAngles[1];     // Assign spaceship rotation with Euler angle
            }
            else
            {
                spaceship.transform.Rotate(Vector3.up, -rotationDegree);
                spaceshipRotY = spaceship.transform.eulerAngles[1];
            }              
        }

        // Assign spaceship position
        spaceship.position = new Vector3(spaceshipPosX, spaceshipPosY, spaceshipPosZ);

        // Space station - spaceship; Do not change the order!
        float x_diff = spaceStationPosX - spaceshipPosX;
        float z_diff = spaceStationPosZ - spaceshipPosZ;
        float r = Mathf.Sqrt(Mathf.Pow(x_diff, 2) + Mathf.Pow(z_diff, 2));
        float stationToZaxisAngleDegree = calcStationAngle(x_diff, z_diff, r);
        float currentAngleDiff = (spaceshipRotY - stationToZaxisAngleDegree) * degree2Rad; // The angle difference between spaceship and space station
        
        //-------------------------- Design reward function ----------------------------------------------
        if (isMove)
        {
            /* 
             * When spaceship distance is closer to the station -> reward!
             * The reward is propotional to the distance (the closer the higher)
             */
            float rewardPosition = (rLimit - r) * 0.1f; //TBD
            if(r < previousR)
                AddReward(rewardPosition);
            else
                AddReward(-rewardPosition);
        }

        if(isRotate)
        {
            /*
             * When spaceship toward the same direction of station -> reward: cos(0)
             * When spaceship toward the opposite direction of station -> punish: cos(180)
             */
            float rewardAngle = Mathf.Cos(currentAngleDiff) * 2.0f; //TBD
            rewardAngle = Mathf.Abs(rewardAngle); //Make it positive

            if (Mathf.Cos(currentAngleDiff) > Mathf.Cos(previousAngleDiff))
                AddReward(rewardAngle);
            else
                AddReward(-rewardAngle);
        }

        /*
         * Punish for too many steps (above 150 steps)
         */
        if ((isMove || isRotate) && stepsCount > 150) //TBD
        {
            AddReward(-stepsCount * 0.001f); //TBD, The punish is propotional to the steps
        }

        // Failure
        if (r >= rLimit || stepsCount > 1000) // Do not waste time
        {
            Done();
            SetReward(-1.0f);
            failureCount++;
            return;
        }
        // Successq
        if (r <= 2)
        {
            Done();
            SetReward(1.0f);
            successCount++;
            return;
        }
        // Tracing
        if (text != null )
        {
            text.text = string.Format("spaceShipPos: ({0}, {1}, {2}), spaceShipRot: (0, {3}, 0)," +
                " spaceSationPos: ({4}, {5}, {6}), spaceStaionAngle: {11}, distance: {7}, reward{12}, total reward: {8}, success: {9}/failure: {10}," +
                " succesRate: {14}%, steps: {13}"
                , spaceshipPosX, spaceshipPosY, spaceshipPosZ, spaceshipRotY
                , spaceStationPosX, spaceStationPosY, spaceStationPosZ, r, GetCumulativeReward(), successCount, failureCount
                , stationToZaxisAngleDegree, GetReward(), stepsCount, (successCount/(successCount+failureCount))*100);
        }
        previousR = r; // reset previous_r
        previousAngleDiff = currentAngleDiff;
        isMove = false;
        isRotate = false;
    }

    public override void AgentReset()
    {
        // Initialize space ship position randomly
        spaceshipPosX = UnityEngine.Random.Range(-initPosRange, initPosRange);
        spaceshipPosZ = UnityEngine.Random.Range(-initPosRange, initPosRange);
        spaceshipRotY = spaceship.transform.eulerAngles[1];
        
        // Assign spaceship position
        //spaceship.position = new Vector3(spaceshipPosX, spaceshipPosY, spaceshipPosZ);

        // Initialize space station position randomly
        spaceStationPosX = UnityEngine.Random.Range(-initPosRange, initPosRange);
        spaceStationPosZ = UnityEngine.Random.Range(-initPosRange, initPosRange);

        // Assign space station position
        spaceStation.position = new Vector3(spaceStationPosX, spaceStationPosY, spaceStationPosZ);

        // Reward clean up
        SetReward(0);
        stepsCount = 0;
        isMove = false;
        isRotate = false;
    }

    public override void AgentOnDone()
    {

    }
}
